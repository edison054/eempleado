public class Operario extends Empleado {
    
    private String Turno;
    private double auxilio;

   
    public String getTurno() {
        return Turno;
    }
     public void setTurno(String Turno) {
        this.Turno = Turno;
    }
    public double getAuxilio() {
        return auxilio;
    }
    public void setAuxilio(double auxilio) {
        this.auxilio = auxilio;
    }


    //constructor default
    public Operario(){
    }
    
    public Operario (String T, double aux){
    super("carlos","ortega", 34, 600000);
        this.Turno=T;
        this.auxilio=aux;
    }
}
