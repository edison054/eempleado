public class Empleado {
 //nombre apellido edad salario   
    private String Nombre;
    private String Apellido;
    private int Edad;
    private double Salario;

    public String getNombre() {
        return Nombre;
    }
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }
    
    public String getApellido() {
        return Apellido;
    }
    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }
    
    public int getEdad() {
        return Edad;
    }
    public void setEdad(int Edad) {
        this.Edad = Edad;
    }
    
    public double getSalario() {
        return Salario;
    }
    public void setSalario(double Salario) {
        this.Salario = Salario;
    }
    
    public void calculars(double p, double o, double i ){
        this.Salario=p+o-i;
    }
    public long mostrarS(){
    return (long) this.Salario;
    }
    //concstructor default
    public Empleado(){
    }
    
    public Empleado(String N, String A, int E, double S){
    this.Nombre=N;
    this.Apellido=A;
    this.Edad=E;
    this.Salario=S;
    }
}
