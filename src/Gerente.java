public class Gerente extends Empleado{
  //sucursal bonificacion aporte
    private String sucursal;
    private double Bonificacion;
    private double aporte;
    public String getSucursal() {
        return sucursal;
    }
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }
    public double getBonificacion() {
        return Bonificacion;
    }
    public void setBonificacion(double Bonificacion) {
        this.Bonificacion = Bonificacion;
    }
    public double getAporte() {
        return aporte;
    }
    public void setAporte(double aporte) {
        this.aporte = aporte;
    }

   //constructr default
   public Gerente(){
   } 
   public Gerente(String s, double B, double a){
       super("andres","insuasti", 43, 1200000);
       this.sucursal=s;
       this.Bonificacion=B;
       this.aporte=a;
   }

}